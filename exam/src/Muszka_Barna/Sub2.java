package Muszka_Barna;

import javax.swing.*;
import java.awt.event.*;

public class Sub2 extends JFrame{

    JTextField a,b,c;
    JButton but;

    Sub2(){

        setTitle("Exam");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,250);
        setVisible(true);
    }

    public void init(){

        this.setLayout(null);
        int width=140;int height = 20;

        a = new JTextField();
        a.setBounds(20,40,width, height);

        b = new JTextField();
        b.setBounds(20,70,width, height);

        c = new JTextField();
        c.setEditable(false);
        c.setBounds(20,100,width, height);


        but = new JButton("Sum no. characters");
        but.setBounds(20,150,width, height);
        but.addActionListener(new Sub2.ButtonEvent());

        add(a);add(b);add(c);add(but);

    }
    class ButtonEvent implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            String strA = Sub2.this.a.getText();
            String strB = Sub2.this.b.getText();
            int a = strA.length();
            int b = strB.length();
            Sub2.this.c.setText(String.valueOf(a+b));

        }
    }
    public static void main(String[] args) {
        new Sub2();
    }
}