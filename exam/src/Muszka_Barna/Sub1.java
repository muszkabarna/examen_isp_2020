package Muszka_Barna;

class A{

}
class B extends A{  //inheritance
    private long t;
    public void b(){

    }

}
class C{
    public void met(B b){  // dependency

    }
}
class D{
    private B b;  // association
    private E e;  // composition
    private F f;  // aggregation
    D(){
        this.e=new E();  // composition
    }
    public void met1(int i){

    }

}
class E{
    public void met2(){

    }

}
class F{
    public void n(String s){

    }

}